#!/bin/bash
echo "Copying image files"
cp ${BINARIES_DIR}/zImage ${BINARIES_DIR}/kernel.bin
cp ${BINARIES_DIR}/rootfs.ubi ${BINARIES_DIR}/rootfs.bin
cp ${BINARIES_DIR}/shc_v1.dtb ${BINARIES_DIR}/devicetree.bin
echo "Images located in ${BINARIES_DIR}"
echo "Flash using <Send File>, in SAM-BA v2.18, unless otherwise noted."
echo "-------------------------------------------------"
echo "|     File         |  Location |     Script     |"
echo "|-----------------------------------------------|"
echo "|   boot.bin       |  0x0      | Send Boot File |"
echo "|   kernel.bin     |  0x200000 |        -       |"
echo "|   devicetree.bin |  0x180000 |        -       |"
echo "|   rootfs.bin     |  0x900000 |        -       |"
echo "|-----------------------------------------------|"
