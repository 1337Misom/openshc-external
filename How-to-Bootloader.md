# How to start the SHC v1 into Bootloader mode
## !!! Do at your own risk !!!
![SHCv1](https://lsh.community/attachment/2792-shcv1-pinout-png/)
1. Disconnect Power and all other Cables
2. Open the Central by unscrewing the 7 Torx T6 screws on the back
3. Short NAND CE to 3.3v using a jumper
4. Attach a Mini-B USB Cable
5. Connect power to the barrel jack.
6. Wait a few seconds
7. Disconnect the jumper from 3.
8. Open SAM-BA
9. Select the tty port from the Central
10. Select `at91sam9g20-ek` as the board and click `Connect`
