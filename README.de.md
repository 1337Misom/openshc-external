# RWE Smarthome SHC external Buildroot
Dieses Repo enthält alle Dateien, die zum Erstellen einer einfachen Linux-Umgebungen auf Basis von BusyBox für die SHC v1 benötigt wird.
## Dieses Buildroot ist nur für Developer gedacht und ist im ALPHA status (alles auf eigene Gefahr)
## Bauen vom Buildroot
### !!! Buildroot erwartet unter Linux zu laufen !!
1. Installiere die basis Pakete, die zum Erstellen benötigt werden (siehe [Buildroot
Anweisungen](https://buildroot.org/downloads/manual/manual.html#requirement))
2. Klone eine aktuelle Buildroot-Version (getestet mit 2024.05-git Commit:
[87943b75](https://gitlab.com/buildroot.org/buildroot/-/tree/87943b75310190db05342232046790db0f8e4232))
3. Klone dieses [Repo](https://gitlab.com/1337Misom/OpenSHC-external.git)
4. Lade [SAM-BA v2.18](https://ww1.microchip.com/downloads/en/DeviceDoc/SAM-BA+v2.18+for+Linux.zip) herunter
5. Gehe in das Buildroot-Verzeichnis: `cd Buildroot`
6. Lade die externe Konfiguration: `make BR2_EXTERNAL=../OpenSHC-external shc_v1_defconfig`
7. Baue das Buildroot: `make -j$(nproc)`
8. Starte die Zentrale in den Bootloader-Modus (siehe `How-to-Bootloader.de.md`)
9. In SAM-BA gehe zu dem `NandFlash` Tab
10. Wähle `Enable NandFlash` in der `Scripts`-Sektion aus und drücke `Execute`
### !!! Ab jetzt wird das originale OS überschrieben !!!
### Wenn du das zum ersten Mal machst, führe `Scrub NandFlash` aus
12. Wähle `Send Boot File` in der `Scripts`-Sektion aus und drücke `Execute`
13. Geben im `Send File Name` Feld den Pfad zu `devicetree.bin` in dem `output/images`-Verzeichnis des Buildroot
ein und drücke `Execute`
14. Setze `Address` auf `0x180000` und drücke `Execute`
15. Wähle `Send File Name` erneut aus und gib den Pfad zu `kernel.bin` in der `output/images`-Verzeichnis des
Buildroot ein
16. Setze `Address` auf `0x200000` und drücke `Execute`
17. Wähle `Send File Name` erneut aus und gib den Pfad zu `rootfs.bin` in der `output/images`-Verzeichnis des
Buildroot ein
18. Setze `Address` auf `0x900000` und drücke `Execute`
19. Verbinde einen UART-Adapter mit den `TX` und `RX` Header des ST500
20. Starte die Zentrale
21. Logge dich mit `root` und `openshc` ein

### Backup vom NAND
1. Bootiere den Central in den Bootloader-Modus (siehe `How-to-Bootloader.md`)
2. In der `NandFlash`-Sektion setze `Receive File Name` auf deine gewünschte Speicherposition
3. Setze `Size (For Receive File)` auf `0xFFFFFFF`
4. Drücke `Receive File`

### Wiederherstellen des NAND
1. Starte die Zentrale in den Bootloader-Modus (siehe `How-to-Bootloader.de.md`)
2. Im `NandFlash` Tab setze `Send File Name` auf die Dump-Datei
3. Setze `Address` auf `0x0`
4. Drücke `Send File`
