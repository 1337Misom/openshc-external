# RWE Smarthome SHC external Buildroot
This project tries to bring open source software on the SHC devices and bringing freedom to the user.
## This Buildroot is only meant for Developers and is in ALPHA state
## DO THIS AT YOUR OWN RISK !!!
This repo includes all Files required to build a simple BusyBox based Linux environment for the SHC v1.
## Build
### !!! Buildroot is expected to run on Linux !!!
1. Install the Basic Packages required for building (follow [Buildroot instructions](https://buildroot.org/downloads/manual/manual.html#requirement))
2. Clone a recent Buildroot Version (tested with 2024.05-git commit: [87943b75](https://gitlab.com/buildroot.org/buildroot/-/tree/87943b75310190db05342232046790db0f8e4232))
3. Clone this [repo](https://gitlab.com/1337Misom/openshc-external.git)
4. Download [SAM-BA v2.18](https://ww1.microchip.com/downloads/en/DeviceDoc/SAM-BA+v2.18+for+Linux.zip)
5. Go into the Buildroot directory: `cd Buildroot`
6. Load the external config: `make BR2_EXTERNAL=../openshc-external shc_v1_defconfig`
7. Build the Buildroot: `make -j$(nproc)`
8. Boot the Central into Bootloader mode (see `How-to-Bootloader.md`)
9. In SAM-BA go to the `NandFlash` tab
10. Select `Enable NandFlash` in the `Scripts` section
11. Press `Execute`
### !!! From now on you will overwrite the original OS !!!
### If you are doing this the first time run `Scrub NandFlash`
12. Select `Send Boot File` in the `Scripts` section and `Execute`
13. At `Send File Name` select the `devicetree.bin` file from the `output/images` folder inside `Buildroot`
14. Set `Address` to `0x180000` and press `Execute`
15. Set `Send File Name` to `kernel.bin`
16. Set `Address` to `0x200000` and press `Execute`
17. Set `Send File Name` to `rootfs.bin`
18. Set `Address` to `0x900000` and press `Execute`
19. Connect a UART adapter to `TX` and `RX` on `ST500`
20. Boot the Central
21. Login using `root` and `openshc`

## Backup NAND
1. Boot the Central into Bootloader mode (see `How-to-Bootloader.md`)
2. In the `NandFlash` tab set `Receive File Name` to your desired location
3. Set `Size (For Receive File)` to `0xFFFFFFF`
4. Press `Receive File`

## Restore NAND
1. Boot the Central into Bootloader mode (see `How-to-Bootloader.md`)
2. In the `NandFlash` tab set `Send File Name` to the flash dump
3. Set `Address` to `0x0`
4. Press `Send File`
