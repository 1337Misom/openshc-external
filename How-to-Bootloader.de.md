# SHC v1 in Bootloader starten
## !!! Alles auf eigene Gefahr !!!
![SHCv1](https://lsh.community/attachment/2792-shcv1-pinout-png/)
## Es wird keine Haftung übernommen beim Beschädigen der Zentrale
1. Strom und alle anderen Kable trennen
2. Die Zentrale durch Entfernen der 7 Torx T6-Schrauben auf der Rückseite öffnen
3. Verbinde NAND CE mit 3.3V
4. SHC v1 starten (Stromkabel anstecken)
5. Warte ein Paar Sekunden
6. NAND CE von 3.3v trennen
7. Öffne [SAM-BA v2.18](https://www.microchip.com/en-us/development-tool/SAM-BA-In-system-Programmer)
8. Wähle `at91sam9g20-ek` als Board auswählen
9. Drücke auf `Connect`
